@extends('layouts.siswa.dashboard')

@section('body')
    <div class="container mt-4" >
        <div class="card" >
            <div class="card-body">
                @if (Auth()->user()->status == null)
                <center>
                    <h3>Anda belum melakukan proses Prakerin Apapun </h3>
                    <a href="/lanjut" class="btn btn-success">Mulai</a>
                </center>
                @elseif (Auth()->user()->status == 'Menunggu Proses Validasi Surat Pengantar')
                    <h3>Anda sedang di tahap {{ Auth()->User()->status }}</h3>
                    <a href="/lanjut">Lanjut ke tahap selanjutnya</a>

                @elseif (Auth()->user()->status == 'Memilih Tempat Prakerin')
                    <h3>Lembar Pengesahan anda sudah di validasi dan di terima oleh pembimbing</h3>
                    <h3>silahkan Pilih Perusahaan untuk melakukan Prakerin</h3>

                    <p>Pesan dari pembimbing:</p><textarea readonly cols="30" rows="3">{{ Auth()->user()->ket_message_pengantar }}</textarea><br>
                    <a href="/download/{{ Auth()->User()->pengantar_pkl }}" class="btn btn-warning btn-xm" >Lihat Surat pengantar</a>

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" >Pilih Tempat Prakerin</button>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Pilih Tempat PKL</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/insertTempatPkl" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{ Auth()->User()->id }}">
                                <input type="hidden" name="status" value="Mengajukan Surat Pengantar ke Perusahaan">
                                <select class="custom-select" id="inputGroupSelect04" name="perusahaan_id">
                                <option selected>Choose...</option>
                                @foreach ($perusahaan as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_perusahaan }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Kirim</button>
                        </form>
                        </div>
                        </div>
                    </div>
                    </div>
                @elseif (Auth()->user()->status == 'Mengajukan Surat Pengantar ke Perusahaan')
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">Silahkan Cetak dan Kirim Surat Pengantar ke Tempat Prakerin yang telah anda pilih</h3>
                        <h4 class="card-title">Nama Perusahaan yang dipilih : {{ Auth()->User()->perusahaan->nama_perusahaan }}</h4>
                        <h4 class="card-title">Alamat Perusahaan yang dipilih : {{ Auth()->User()->perusahaan->alamat_perusahaan }}</h4>
                        <form action="/download" method="post">
                            @csrf
                            <input type="hidden" name="pengantar_pkl" value="{{ Auth()->User()->pengantar_pkl }}">
                            <input type="submit" class="btn btn-warning" value="Cetak Pengantar Surat Pengantar">
                        </form>
                    </div>
                </div>
                <div class="card" >
                    <div class="card-body">
                        <p class="card-text">Jika Anda Sudah Mengirimkan Surat Pengantar Klik Tombol dibawah ini!</p>
                        <form action="/gantiStatus" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{ Auth()->User()->id }}">
                            <input type="hidden" name="status" value="Masa Sanggah balasan surat Pengantar/pegajuan">
                            <input type="submit" class="btn btn-success" value="Saya Sudah Mengirimkan Surat Pengantar ke Perusahaan tempat Prakerin">
                        </form>
                    </div>
                </div>
                @elseif (Auth()->user()->status == 'Masa Sanggah balasan surat Pengantar/pegajuan')
                <div class="card" >
                    <div class="card-body">
                        <h4>Anda Sedang dalam Masa sanggah Menunggu balasan surat pengantar/pengajuan dari perusahaan {{ Auth()->User()->perusahaan->nama_perusahaan }}</h4>
                        <label for="">Tekan tombol dibawah jika menerima surat balasan pengajuan dari perusahaan yang diajukan</label><br>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Konfirmasi Surat balasan pengajuan</button>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="/konfirmasiPengajuan" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{ Auth()->user()->id }}">
                                <div class="mb-3">
                                <label for="" class="form-label">NIS</label>
                                <input type="text"
                                    class="form-control" name="" id="" aria-describedby="helpId" value="{{ Auth()->User()->nis }}" readonly>
                                </div>
                                <div class="mb-3">
                                <label for="" class="form-label">Nama Lengkap</label>
                                <input type="text"
                                    class="form-control" name="" id="" aria-describedby="helpId" value="{{ Auth()->User()->name }}" readonly>
                                </div>
                                <div class="mb-3">
                                <label for="" class="form-label">Perusahaan yang di ajukan</label>
                                <input type="text"
                                    class="form-control" name="" id="" aria-describedby="helpId" value="{{ Auth()->User()->perusahaan->nama_perusahaan }}" readonly>
                                </div>
                                <div class="mb-3">
                                <label for="" class="form-label">Konfirmasi Balasan Dari perusahaan yang diajukan </label>
                                <select class="form-control" name="status" id="" style="height: 30px" required>
                                    <option value=""></option>
                                    <option value="Pengajuan Prakerin Disetujui">Disetujui</option>
                                    <option value="Pengajuan Prakerin Ditolak">Ditolak</option>
                                </select>
                                <div class="mb-3">
                                <label for="" class="form-label">Mulai PKL</label>
                                <input type="date"
                                    class="form-control" name="mulai_pkl" id="" aria-describedby="helpId" value="{{ Auth()->User()->perusahaan->nama_perusahaan }}" >
                                </div>
                                <div class="mb-3">
                                <label for="" class="form-label">Selesai PKL</label>
                                <input type="date"
                                    class="form-control" name="selesai_pkl" id="" aria-describedby="helpId" value="{{ Auth()->User()->perusahaan->nama_perusahaan }}">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                            </div>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                </div>
                @elseif (Auth()->user()->status == 'Pengajuan Prakerin Disetujui')
                <h4>Pengajuan Prakerin disetujui</h4>
                {{-- <h5>Nama Pembimbing Internal : {{ Auth()->user()->pembimbing->name }}</h5> --}}
                <h5>Admin Sedang memilih pembimbing untuk anda</h5>
                <h5>Mulai melakukan prakerin pada tanggal: {{ Auth()->user()->mulai_pkl }} - {{ Auth()->user()->selesai_pkl }}</h5>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                    Baca Tata Tertib Prakerin!
                </button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Tata tertib Prakerin</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            TATA TERTIB PKL/PRAKERIN SMK MUHAMMADIYAH LOA KULU TAHUN 2021
                            Tata Tertib Prakerin disusun sebagai pedoman siswa Prakerin untuk dapat berbuat, bertindak dan berperilaku demi kelancaran pelaksanaan dan keberhasilan Prakerin di DU/DI.
                            Tata tertib ini mengatur kegiatan siswa saat prapelaksanaan dan selama pelaksanaan di lokasi Prakerin.

                            PRA PELAKSANAAN
                            A. Pembekalan
                            • Siswa calon peserta Prakerin wajib mengikuti semua kegiatan pealan yang telah ditentukan sesuai dengan jadwal dan atau perubahan atau tambahannya.
                            • Setiap sesi kegiatan pembekalan dilakukan presensi yang harus ditandatangani oleh siswa calon peserta Prakerin. Presensi pembekalan merupakan salah satu prasyarat siswa ke lapangan DU/DI.
                            • Siswa calon peserta Prakerin bertanggung jawab atas diri pribadi masing-masing. Apabila ada tanda tangan yang dipalsukan atau terjadi kelebihan tanda tangan, maka presensi kedua belah pihak dinyatakan tidak berlaku.
                            • Selama mengikuti pembekalan, siswa calon peserta Prakerin wajib menjaga ketertiban dan bersikap tenang.
                            • Petugas pembekalan berhak menegur, mencatat atau mengeluarkan siswa calon peserta Prakerin yang mengganggu kelancaran kegiatan pembekalan dan oleh dihapus dari presensi.

                            B. Konsolidasi
                            • Siswa calon peserta Prakerin wajib mengikuti semua kegiatan konsolidasi dengan pembimbing internal masing-masing.
                            • Siswa calon peserta Prakerin wajib menandatangani presensi. Presensi dan aktivitas konsolidasi merupakan komponen penilaian.

                            PELAKSANAAN
                            Selama pelaksanaan Prakerin siswa wajib :
                            • Menjaga nama baik DU/DI dan sekolah.
                            • Mentaati peraturan sekolah.
                            • Mengikuti seluruh prosesi penerjunan dan penarikan sesuai dengan jadwal yang telah ditetapkan.
                            • Mengikuti semua tata tertib yang ada di tempat Pakerin.
                            • Berperilaku dan bersikap baik, bertanggung jawab, sopan, santun dan jujur.
                            • Menetap di lokasi Prakerin atau sesuai ketentuan.
                            • Melaksakan K3 (Kesehatan dan Keselamatan Kerja).
                            • Melaksanakan tugas pokok dan fungsi yang disepakati
                            • Melaksanakan tugas-tugas Prakerin dengan penuh rasa tanggung jawab dan dedikasi yang tinggi, baik tugas administrasi, yaitu pengisian presensi harian, pelaksanaan kegiatan dan penulisan laporan kegiatan/pelaksanaan
                            • Menghayati dan menyesuaikan diri dengan kehidupan dilokasi Prakerin.

                            Membina kerjasama dengan sesama siswa, karyawan di lingkungan prakerin, instansi/dinas pemerintah dan pihak-pihak yang terkait.
                            • Menjaga kelengkapan dan keutuhan semua atribut (Kartu Tanda Pengenal, pakaian praktek dan perlengkapan).
                            • Menjaga seluruh barang/harta pribadi yang dibawa ke lokasi Prakerin segala kerusakan dan kehilangan barang/harta pribadi di lokasi menjadi tanggung jawab peserta prakerin.
                            • Mengikuti responsi yang dilakukan oleh guru pebimbing dan pembimbing eksternal secara tertulis dan atau lisan pada akhir pelaksanaan.
                            • Mematuhi segala peraturan yang berlaku dan mematuhi setiap instruksi di tempat kerja dalam perusahaan atau tempat melaksanakan program prakerin.
                            • Berada ditempat praktek 15 menit sebelum praktek dimulai, berlaku sopan, jujur, bertanggung jawab, berinisiatif, kreatif terhadap tugas-tugas yang diberikan dalam praktek.
                            Memakai pakaian seragam sekolah, dan dalam keadaan tertentu memakai pakaian praktek. Tidak dibenarkan memakai pakaian bebas.
                            Memberi salam pada waktu datang dan memohon diri pada waktu akan pulang.
                            • Memberitahukan kepada Pimpinan / Pembimbing eksternal jika berhalangan hadir atau bermaksud untuk meninggalkan tempat praktek.
                            • Membicarakan dengan segera kepada pembimbing eksternal, ketua kelompok atau petugas yang ditunjuk apabila mengalami kesulitan.
                            • Mentaati peraturan dalam penggunaan peralatan dan bahan yang akan dipakai dalam praktek.
                            • Melaporkan dengan segera kepada yang berwenang bila terjadi kerusakan/salah dalam pelaksanaan praktek.
                            • Membersihkan dan mengatur kembali peralatan dengan rapi seperti semula setelah melakukan praktek.
                            • Melakukan observasi dan penelitian yang mempunyai tujuan positif.
                            • Bertanya kepada pihak yang kompeten apabila kurang paham/tidak mengerti.

                            Selama pelaksanaan Prakerin siswa dilarang :
                            • Melakukan perbuatan yang mencemarkan nama baik sekolah
                            • Melakukan tindakan asusila
                            • Merokok ditempat praktek.
                            • Mempergunakan pesawat telepon atau peralatan lainnya tanpa seijin perusahaan.
                            • Melakukan perbuatan dan kegiatan yang melanggar hukum secara langsung maupun tidak langsung
                            • Menerima tamu pribadi sewaktu melaksanakan praktek.
                            • Membawa keluarga atau teman ikut menginap di lokasi Prakerin tanpa ijin dari Ketua Prakerin
                            • Pindah tempat kegiatan praktek kecuali atas perintah yang berwenangdalam mengatur kegiatan praktek.
                            • Menggunakan wewenang di luar status peserta Prakerin
                            

                            SANKSI AKIBAT PELANGGARAN TATA TERTIB
                            Sanksi akibat pelanggarakan tata tertib diberikan dalam bentuk Peringatan Tingkat l, ll dan lll.
                            Peringatan Tingkat I
                            Peringatan Tingkat I dberikan terhadap siswa yang melakukan satu atau lebih pelanggaran sebagai berikut :
                            • Tidak mengikuti kegiatan konsolidasi tanpa ijin
                            • Tidak mengisi Lembar Pelaksanaan kegiatan Prakerin
                            • Tidak mengisi presensi harian yang telah disediakan atau mengisi presensi harian melebihi hari yang sedang berjalan
                            • Meninggalkan lokasi tanpa ijin dan atau tanpa diketahui rekan siswa dalam satu kelompok.
                            • Tidak menggunakan atribut selama melaksanakan Prakerin
                            • Tidak mengikuti prosesi penerjunan atau penarikan tanpa ijin.

                            Peringatan Tingkat ll
                            Peringatan Tingkat ll diberikan terhadap siswa peserta Prakerin yang melakukan satu atau lebih pelanggaran sebagai berikut :
                            • Telah diberi Peringatan Tingkat l, tetapi masih melakukan pelanggaran
                            • Berdasarkan pertimbangan pembimbing internal, rekan siswa peserta Prakerin dan dari pertimbangan DU/DI, siswa dianggap tidak dapat menghayati dan menyesuaikan diri di lingkungan Prakerin.
                            • Meninggalkan lokasi kerja tanpa ijin.
                            • Membawa keluarga atau teman ikut menginap di lokasi Prakerin tanpa ijin dari pembimbing internal, sekretaris prakerin, koordinator operasional dan monitoring atau ketua Prakerin
                            • Tidak bisa bekerjasama dengan sesama siswa, karyawan DU/DI, instansi atau dinas pemerintah dan pihak-pihak yang terkait dengan pelaksanaan Prakerin.

                            Catatan:
                            Peringatan I dan ll menentukan nilai yang direkomendasikan oleh pembimbing internal.

                            Peringatan Tingkat lll
                            Peringatan Tingkat lll diberikan kepada siswa yang melakukan satu atau lebih pelanggaran sebagai berikut :
                            • Telah diberi peringatan Tingkat ll, tetapi masih melakukan pelanggaran
                            • Melakukan perbuatan yang mencemarkan nama baik sekolah
                            • Meninggalakan lokasi Prakerin dua hari berturut-turut
                            • Melakukan perbuatan yang dikategorikan sebagai tindakan melanggar hukum, asusilia atau kegiatan yang meresahkan karyawan dilokasi Prakerin maupun diluar lokasi.
                            • Melakukan segala perbuatan yang bersifat pemalsuan atau penipuan administratif, yaitu:
                            a. Pemalsuan tanda tangan pembimbing internal maupun pembimbing eksternal pada lembar kegiatan Prakerin
                            b. Pemalsuan tanda tangan pada buku laporan dan sebagainya.
                            c. Pemalsuan dan atau penipuan identitas.


                            Sanksi Peringatan Tingkat lll ini berupa :
                            • Siswa tersebut diminta untuk mengundurkan diri sebagai peserta Prakerin dan mengulang tahun depan.
                            • Penarikan dari lokasi prakerin sehingga dinyatakan tidak lulus.
                            • Penggagalan prakerin (dinyatakan tidak lulus program prakerin).
                            • Merekomendasikan kepada Wakil Sekolah bidang pendidikan dengan tembusan Kepala Sekolah agar siswa tersebut diberikan sanksi sekolah lainnya.
                            • Dikeluarkan dari sekolah.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Saya Sudah membaca</button>
                        </div>
                    </div>
                </div>

                @elseif (Auth()->user()->status == 'Sudah Mendapatkan Pembimbing')
                    <h3>Admin Sudah menetapkan pembimbing untuk anda</h3>
                    <h5>Silahkan Mulai PKL Pada tanggal : {{ Auth()->User()->mulai_pkl }} - {{ Auth()->User()->selesai_pkl }}</h5>
                    <h5>Nama Pembimbing : {{ Auth()->User()->pembimbing->name }}</h5>
                    <a href="/jurnal">Isi Jurnal Harian</a>
                @elseif (Auth()->user()->status == 'Pengajuan Prakerin Ditolak')
                    <div class="alert alert-danger" role="alert">
                    Pengajuan PRAKERIN di {{ Auth()->user()->perusahaan->nama_perusahaan }} <strong>DITOLAK</strong>
                    </div>
                    <h4>Silahkan Membuat kembali Surat pengantar/ pengajuan baru</h4>
                    <form action="/gantiStatus" method="post">
                        @csrf
                        <input type="hidden" name="status" value="">
                        <input type="hidden" name="id" value="{{ Auth()->user()->id }}">
                        <input type="submit" value="Buat Surat Pengantar/ pengajuan baru">

                    </form>
                @else
                    <h4>cek</h4>
                @endif
            </div>        
        </div>
                <button onclick="kembali()" class="btn btn-danger">Kembali</button>
                <script>function kembali(){
                    window.history.back();
                }</script>
    </div>

@endsection